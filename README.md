# pvrports
## Unofficial PostmarketOS repository for 5th Generation PowerVR devices

This unofficial repository has some tweaks to support GPU acceleration for 5th Generation PowerVR devices.

## Catalog

Our catalog supports the Samsung Galaxy Tab 2 tablet (samsung-espresso - 7" models - P3100, P3110, P3113 and samsung-espresso10 - 10.1" models - P5100, P5110, P5113), but more devices could be added in the future, as long as there are some active maintainer for it.

## Importing this repository

Importing this repository is easy, as long as you have pmbootstrap installed, up and running.

You just have to copy this content inside your pmbootstrap work repository (by default it is located in `$HOME/.local/var/pmbootstrap/cache_git/pmaports`)

Copy each folder to its respective location in pmaports:

`community/device-samsung-espresso7` will replace `device/community/device-samsung-espresso7` in pmaports.

`community/device-samsung-espresso10` will replace `device/community/device-samsung-espresso10` in pmaports.

`community/linux-openpvrsgx` should be copied to `device/community/` in pmaports.

`community/libglvnd` should be copied to `main/` in pmaports.

`community/mesa-pvr-dri-classic` should be copied to `main/` in pmaports.

`community/phoc` should be copied to `main/` in pmaports.

`non-free` should be copied directly to pmaports.

After that, you will need to run `pmbootstrap init` again, and select one of the devices we offer support (espresso7 or espresso10).

## Importing our custom package repository for easy upgrades

We offer a package called `pvrports-keys`, which imports an essential key for easy package upgrades, hosted in a custom repository (https://pvrports.antonialoytorrens.com).

You will have to add the following content into `/etc/apk/repositories`:

<pre>
https://pvrports.antonialoytorrens.com/pvrports/$PMOS_VERSION
</pre>

Where $PMOS_VERSION is the PostmarketOS version you run on your device (e.g.: v23.06, v22.12, v21.12, etc.).

You can double-check the PostmarketOS version by running `/etc/issue` on the terminal, or in the *About* section on Gnome Settings / Plasma Settings).

---

If you already have a running system, you might want to install it directly by running `apk add --allow-untrusted pvrports-keys` and skip the entire section below.

The following UI are known to work with GPU acceleration (do note that GPU Acceleration will not work with X11/X.Org based UIs, only Wayland is supported):

### PostmarketOS v23.06, Alpine 3.18 (recommended)

Each UI needs the non-free SGX binaries to be installed to get the GPU working.
We do not use mainline mesa, but instead use the mesa-amber branch due to them dropping support for classic drivers. https://docs.mesa3d.org/amber.html. Hence we replace mainline mesa with mesa amber and pvr patches on top, and require glvnd.

#### Installation on userdata
Installing on userdata with zip : `pmbootstrap install --android-recovery-zip --recovery-install-partition=data`

Please copy the zip to a sdcard and unmount data before installing from TWRP.

This will erase everything (not TWRP) including the previous OS, images, media etc.

#### Installation on SD Card
Installing on sdcard : `pmbootstrap install --sdcard=/dev/SDCARD`

Insert sdcard to your computer and pmOS will be installed there.

Then insert sdcard to device and flash boot image present in it to boot into the OS on sdcard. (Only required once). You can dual boot in this manner by flashing the boot.img of the corresponding OS.

#### Phosh

Phosh needs custom patched phoc (due to a patch needed in wlroots 0.15).
No extra setup needed, just select phosh from the list of UI.
Phosh cannot be installed on /SYSTEM partition, hence it needs to be installed on userdata or in a sdcard.

#### Plasma-Mobile

Plasma Mobile does not require any extra packages.
Plasma Mobile cannot be installed on /SYSTEM partition, hence it needs to be installed on userdata or in a sdcard.

#### Gnome-Mobile

Gnome-Mobile does not require any extra packages.
Gnome Mobile cannot be installed on /SYSTEM partition, hence it needs to be installed on userdata or in a sdcard.
