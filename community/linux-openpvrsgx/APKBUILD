# Maintainer: Mighty <mightymb17@gmail.com>
# Co-Maintainer: Antoni Aloy <aaloytorrens@gmail.com>
pkgname=linux-openpvrsgx
pkgver=6.1.0
pkgrel=1
pkgdesc="Mainline kernel for PowerVR devices"
arch="armv7"
_carch="arm"
_flavor="openpvrsgx"
url="https://kernel.org"
license="GPL2"
options="!strip !check !tracedeps pmb:cross-native pmb:kconfigcheck-community"
makedepends="openssl-dev yaml-dev mpc1-dev mpfr-dev xz findutils bison flex perl sed bash gmp-dev bc linux-headers elfutils-dev"
_commit="7c72e56fd0a27f87a2f5716a799f6f9d9005c016"
_config="config-$_flavor.$arch"
source="
	0001-arm-dts-Add-TWL6032-dtsi.patch
	0002-arm-dts-Add-common-dtsi-for-espresso.patch
	0003-arm-dts-Add-espresso7-support.patch
	0004-arm-dts-Add-espresso10-support.patch
	0005-Add-TWL6030-power-driver-with-minimal-support-for-po.patch
	0006-Add-TWL6030-power-button-support-to-twl-pwrbutton.patch
	0007-mfd-add-STMPE811-ADC-only-driver.patch
	0008-Add-clk32kg-to-twl6030.patch
	0009-drivers-power-Add-support-for-smb136-charger.patch
	0010-Revert-partially-Revert-usb-musb-Set-the-DT-node-on-the-child-device.patch
	0011-bugfix-ata-ahci-fix-enum-constants-for-gcc-13.patch
	0012-bugfix-gcc-plugins-drop-std-flag.patch
	0013-bugfix-gcc-plugins-reorganize-gimple-includes-for-gcc-13.patch
	$pkgname-$_commit.tar.gz::https://github.com/tmlind/linux_openpvrsgx/archive/$_commit.tar.gz
	$_config
"
builddir="$srcdir/linux_openpvrsgx-$_commit"
provides="linux-samsung-espresso3g"

prepare() {
	default_prepare
	cp "$srcdir/config-$_flavor.$arch" .config
}

build() {
	unset LDFLAGS
	make V=1 ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-postmarketOS"
}

package() {
	install -Dm644 "$builddir/arch/$_carch/boot/"*zImage \
		"$pkgdir/boot/vmlinuz"

	install -D "$builddir"/include/config/kernel.release \
		"$pkgdir"/usr/share/kernel/$_flavor/kernel.release

	make modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_DTBS_PATH="$pkgdir/usr/share/dtb"
}

sha512sums="
f3657e66872059c1c66375504d1dd581ee58126c48dd895a6520474db544263611d9ecc19900fb2e311c86bac8c84610ebd18f8f0b09fab54e64a3ccd622926d  0001-arm-dts-Add-TWL6032-dtsi.patch
92934fd66a8e9a300164481cdfeb7ce1c351961d017a3ff23ac6dfa0fbe63c14f68a6524cac21abce72f183002fca1ce285edba327e7ff8cceb34c4764527958  0002-arm-dts-Add-common-dtsi-for-espresso.patch
aa006d419115c2dcb45ad61e2644659249f1f21d54fc1bc82e97f33b3e017e0f343e75141a40285894e48c3f8d7f0cae0dac0c400d92860ce4b4eb0e3573c903  0003-arm-dts-Add-espresso7-support.patch
e956a466d33373e1cd1d7906d233a12d47b765d7a403ab59395aa70318f74f4b96a13efc3cbeffabd0ccce442d06188348b8830f20653488e7c3a595f10bb712  0004-arm-dts-Add-espresso10-support.patch
fdbd84d6f98d4896dbfe15b24f3c2823c404c6ff95016a73fd395c0da4141fbeab656a1d7d0c01382f73d071eca428b128a2aaa96f7be8313903a150423f2b9e  0005-Add-TWL6030-power-driver-with-minimal-support-for-po.patch
4d8fed617dc0b55654905ac3942ab4d2619d1e381a80e54d844961e8034edba6b71b5a9b2b33322533d654bc06c83960f320e3beb980173280cd897e74a17441  0006-Add-TWL6030-power-button-support-to-twl-pwrbutton.patch
91ba4788d09db8496a6f7b33ff64134e342229c5316fb69be7f5ad8514ebd096293cc2615d50e00e5745f920df79d270ae88ba4b596c789d3a906f312164073d  0007-mfd-add-STMPE811-ADC-only-driver.patch
f13744ed3636383953e9b68b14bef21c79c2a6b135bace32adbed3c28f079724963c148551bb2cdf6ec846f23b6f63d2a75dd79a0359c81af8cbfc745aef225a  0008-Add-clk32kg-to-twl6030.patch
812ac68b8eefb320cd2dd7df7bdae09e26aeead15561d7d7a22d3118643a34c1e3a20a2272703923510abd125c412c390fd0f7702bfe803f47c01176709a0a6b  0009-drivers-power-Add-support-for-smb136-charger.patch
5dea3463f13ebdaf17535a59ee5ffd98ad6d9d3750f6bcafc527699ead6cc4e6a3cf2aa161419c56d4bcec3a51ac2e3427a7883ce26137bec91440239645ec1e  0010-Revert-partially-Revert-usb-musb-Set-the-DT-node-on-the-child-device.patch
a5495ed745e138c33943b31a7c80e33aac03d17ef0a5be6f6ebf1bfe66154b37cff6fd501b255e19d80c7773218624f23c907928c092277a869d25a4053d3d6e  0011-bugfix-ata-ahci-fix-enum-constants-for-gcc-13.patch
44cf33772426c5bc0e97f819831366e1312245b4558febb5a61ac8282f1dc53ed9f415193bbcb23ca4505976fa05959fa359aebfc6b1cfa4523c8723eec62cdc  0012-bugfix-gcc-plugins-drop-std-flag.patch
a56ffb85c04c92dba790373268ab6daa4618b4e98fd1b7f3715a2270622f98331a4f9f82f69d271b412e1e49be67d55dd68fcb12460ae099c595c00568278c50  0013-bugfix-gcc-plugins-reorganize-gimple-includes-for-gcc-13.patch
d3d0005ccfe0f7af19482fea53543dc59e1081862d3a6fa624e53fc54cdbedd78f570a5386f1610f223d5a3ec2402107c7acc678ef500dbb47d43b7177ede0ae  linux-openpvrsgx-7c72e56fd0a27f87a2f5716a799f6f9d9005c016.tar.gz
8899a86adb2020f5e6ebf5342bfdc569bb310380f650825ec3a53133bd18c66df5ef6dc9b455d92a5f5ecde8982321a3793f6b02bd699f168ab4b14298aa5583  config-openpvrsgx.armv7
"
