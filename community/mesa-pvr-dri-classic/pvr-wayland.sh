#!/bin/sh

# We need to explicitly set driver to pvr for the newer mesa, else it tries to find omapdrm
export MESA_LOADER_DRIVER_OVERRIDE=pvr

# Set that we are using amber
export __GLX_VENDOR_LIBRARY_NAME='amber'

# Debug environment variables for easier troubleshooting
export MESA_DEBUG=1
export LIBGL_DEBUG=verbose
export EGL_LOG_LEVEL=debug
export MESA_LOG_FILE=/var/log/mesa.log
